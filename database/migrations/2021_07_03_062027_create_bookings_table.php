<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->integer('shop_id')->default(1)->nullable();
            $table->unsignedBigInteger('customer_id')->index();
            $table->unsignedBigInteger('location_id')->nullable();
            $table->unsignedBigInteger('agent_id')->nullable();
            $table->dateTime('start_date_time');
            $table->dateTime('end_date_time');
            $table->mediumInteger('buffer_before')->nullable();
            $table->mediumInteger('buffer_after')->nullable();
            $table->mediumInteger('duration')->nullable();
            $table->mediumInteger('price')->nullable();
            $table->string('status')->nullable();
            $table->string('customer_comment')->nullable();
            $table->string('coupon_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
