<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('App\Http\Controllers\Api')->group( function (){
    Route::post('register', 'RegisterController@register');
    Route::post('login', 'RegisterController@login');
    Route::post('forgot-password', 'RegisterController@forgotPassword');

    Route::middleware('auth:sanctum')->group( function () {
        Route::get('profile', 'RegisterController@userDetail');
        Route::post('profile-update', 'RegisterController@profileUpdate');
        Route::post('change-password', 'RegisterController@changePassword');
        Route::get('booking-list','BookingController@index');
        Route::post('store-booking','BookingController@storeBooking');
        Route::get('available-slots','BookingController@available_booking_slots');
        Route::get('services','ServicesController@index');
        Route::get('service-detail/{id}','ServicesController@detail');
        Route::get('total-orders','BookingController@totalOrders');
        Route::get('jobs-completed','BookingController@jobsCompleted');
        Route::get('upcoming-appointments','BookingController@upcomingAppointments');
        Route::get('previous-appointments','BookingController@previousAppointments');
        Route::get('dashboard','DashboardController@index');
        Route::get('about','AboutController@index');

    });
});

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

