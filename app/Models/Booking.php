<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    protected  $fillable = ['start_date','end_date','buffer_before','buffer_after',
        'customer_id','location_id','agent_id','duration'
    ,'price','status','customer_comment','coupon_code','service_id','start_time','end_time'];

    public function service(){
        return $this->belongsTo(Services::class,'service_id');
    }

    public function customer(){
        return $this->belongsTo(User::class,'customer_id');
    }

    public function getStartTimeAttribute($value)
    {
        return Carbon::parse($value)->format('g:i A');
    }
    public function getEndTimeAttribute($value)
    {
        return Carbon::parse($value)->format('g:i A');
    }
}
