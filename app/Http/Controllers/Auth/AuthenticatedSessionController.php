<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $authenticate = $request->authenticate();
       if($authenticate === false && $authenticate != null){
          $errors =  ValidationException::withMessages([
               'email' => __('auth.failed'),
           ]);
           return new JsonResponse(['status'=>422,'message'=>$errors->getMessage()]);
       }else{
           $request->session()->regenerate();
           if($request->ajax()){
               return response()->json(['status' =>200,'message'=>'Login Success' ,'redirct_url' => url('dashboard')]);
           }else{

               return redirect()->intended(RouteServiceProvider::HOME);
           }
       }
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
