<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AboutController extends BaseController
{
    public function index(){
        $total_orders = $this->totalOrders();
        $completed_jobs = $this->jobsCompleted();
        $about_text = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum';
        $path = url('/assets/cutting').'/';
        for($i = 1; $i<= 6; $i++){
            $gallary_images[] = $path.'/'.$i.'.jpg';
        }
        $result_array = [
            'total_orders' => $total_orders,
            'completed_jobs' => $completed_jobs,
            'about_text' => $about_text,
            'gallary_images' => $gallary_images,
        ];
        return $this->sendResponse($result_array, 'About section listed');

    }

    public function totalOrders(){
        try{
            $customer_id = Auth::user()->id;
            $customer_orders = Booking::where('customer_id',$customer_id)->count();
            return $customer_orders;
        }catch (\Exception $exception){
            return $this->sendCatchResponse($exception->getMessage(), []);
        }
    }
    public function jobsCompleted(){
        try{
            $customer_id = Auth::user()->id;
            $customer_orders = Booking::where('customer_id',$customer_id)->where('status','completed')->count();
            return  $customer_orders;
        }catch (\Exception $exception){
            return $this->sendCatchResponse($exception->getMessage(), []);
        }
    }
}
