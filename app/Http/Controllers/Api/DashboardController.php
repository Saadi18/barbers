<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends BaseController
{
    public function index(){
        try {
            $services = Services::where('status','active')->get();
            $popular_cuts = Booking::with('service')->select('id','service_id',DB::raw('COUNT(service_id) AS service_count'))
                ->groupBy('service_id')
                ->orderBy('id', 'DESC')
                ->limit(10)
                ->get();
            $result = [
                'services' => $services,
                'popular_cuts' => $popular_cuts,
            ];
            return $this->sendResponse($result, 'Dashboard Listed');
        }catch (\Exception $exception){
            return $this->sendCatchResponse($exception->getMessage(), []);
        }
    }
}
