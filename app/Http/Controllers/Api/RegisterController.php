<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use http\Message;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'f_name' => 'required',
                'l_name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'c_password' => 'required|same:password',
            ]);

            if($validator->fails()){
                return $this->sendError($validator->errors()->first(), []);
            }

            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $success['token'] =  $user->createToken('barber_app')->plainTextToken;
            $success['name'] =  $user->f_name;

            return $this->sendResponse($success, 'User register successfully.');
        }catch (\Exception $exception){
            return $this->sendCatchResponse('Something went wrong', ['error'=> $exception->getMessage()]);
        }

    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('barber_app')->plainTextToken;
            $success['profile'] =  $user;
            return $this->sendResponse($success, 'User login successfully.');
        }
        else{
            return $this->sendError('Unauthorised.', ['error'=>'Credentials did not matched']);
        }
    }

    public function userDetail(Request $request){
        try {
           $user = auth()->user();
//           $user['image'] = url('assets/users/images/').'/'.$user->image;
            return $this->sendResponse($user, 'Profile Detail.');

        }catch (\Exception $exception){
            return $this->sendCatchResponse('true.', $exception->getMessage().'<br>'.$exception->getTrace());
        }
    }

    public function forgotPassword(Request $request){
        try {
            $input = $request->all();
            $rules = array(
                'email' => "required|email",
            );
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
//                $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
                return $this->sendError($validator->errors()->first(), []);
            } else {
                try {
                    $token = Str::random(8);
                    $user = User::where('email', $request->only('email'))->first();
                    $userEmail = $user->email;
                    $check =  Mail::send('emails.forgot_password', ['userName' => $user->name , 'userEmail' => $userEmail, 'resetRoute' => $token],
                        function ($message) use ($userEmail) {
                            $message->to($userEmail);
                            $message->subject('Forgot Password Email');
                        });
                    $user->password = bcrypt($token);
                    $user->update();
                } catch (\Swift_TransportException $ex) {
//                    dd(1,$ex->getMessage(),$ex->getFile(),$ex->getLine());
                    $arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
                } catch (\Exception $ex) {
//                    dd(2,$ex->getMessage(),$ex->getFile(),$ex->getLine());
                    $arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
                }
            }
            return $this->sendResponse([], 'Forgot Password email sent successfully');
        }catch (\Exception $exception){
            return $this->sendCatchResponse('Server Error', $exception->getMessage().'<br>'.$exception->getTrace());
        }
    }

    public function changePassword(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'current_password' => 'required',
                'password' => 'required',
                'c_password' => 'required|same:password',
            ]);

            if($validator->fails()){
                return $this->sendError($validator->errors()->first(), []);
            }
            $user = Auth::user();
            if (!Hash::check($request->current_password, $user->password))
                return $this->sendError('Current Pssword didnot matched our record', []);

            $user->password = bcrypt($request->password);
            if($user->update())
                return $this->sendResponse([], 'Password Changed successfully.');

            return $this->sendError('Something went wrong', []);

        }catch (\Exception $exception){
            return $this->sendCatchResponse($exception->getMessage().'<br>'.$exception->getTrace(),'Error Exception');

        }
    }

    public function profileUpdate(Request $request){
        try {
            $input = $request->all();
            $rules = array(
                'f_name' => "required|max:16",
                'l_name' => "required|max:16",
                'phone_no' => "required",
                'image' => "required",
            );
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
//                $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
                return $this->sendError($validator->errors()->first(), []);
            }
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $fileExtension = substr(strrchr($image->getClientOriginalName(), '.'), 1);
                if ($fileExtension != 'jpg' && $fileExtension != 'jpeg' && $fileExtension != 'png' && $fileExtension != 'gif') {
                    return $this->sendError('Images only allowed with jpg,jpeg,png extension', []);
                }
                $filesize = \File::size($image);
                if ($filesize >= 1024 * 1024 * 10) {
                    return $this->sendError('File size greater than 10 mb', []);
                }
            }
            $image_url = null;
            if($image = $request->file('image')){
                $image_name = Str::random(10);
                $ext = strtolower($image->getClientOriginalExtension()); // You can use also getClientOriginalName()
                $image_full_name = $image_name.'.'.$ext;
                $upload_path = public_path('assets/users/images/');    //Creating Sub directory in Public folder to put image
                $image_url = $upload_path.$image_full_name;
                $success = $image->move($upload_path,$image_full_name);
            }

            $user = Auth::user();
            $user->f_name = $request->get('f_name');
            $user->l_name = $request->get('l_name');
            $user->phone_no = $request->get('phone_no');
            $user->image = $image_full_name;
            $user->save();
            return $this->sendResponse($user, 'Profile Updated Successfully');

        }catch (\Exception $exception){
            return $this->sendCatchResponse('Server Error', $exception->getMessage().'<br>'.$exception->getTrace());
        }
    }
}
