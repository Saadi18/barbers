<?php
namespace App\Http\Controllers\Api;

use App\Models\Booking;
use Carbon\Carbon;
use Faker\Provider\Base;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class BookingController extends BaseController{

    public function index(){
        try {
            $user_id = Auth::user();
            $bookings = Booking::where('customer_id','=',$user_id)->get();
            return $this->sendResponse($bookings, 'Booking Listed');
        }catch (\Exception $exception){
            return $this->sendCatchResponse($exception->getMessage(), []);
        }
    }
    public function allBooking(){
        try {
            $bookings = Booking::with('customer')->get();
            return $this->sendResponse($bookings, 'Booking Listed');
        }catch (\Exception $exception){
            return $this->sendCatchResponse($exception->getMessage(), []);
        }
    }

    public function available_booking_slots(Request $request){
        try {
            $booking_date = $request->get('booking_date');
            $bookings = Booking::where('start_date', '=',$booking_date)->where('end_date',$booking_date)->get()->toArray();
            $time_slots = ['09:00 AM - 10:00 AM','10:00 AM - 11:00 AM','11:00 AM - 12:00 PM','12:00 PM - 01:00 PM',
                '01:00 PM - 02:00 PM','02:00 PM - 03:00 PM','03:00 PM - 04:00 PM',
                '04:00 PM - 05:00 PM','05:00 PM - 06:00 PM','06:00 PM - 07:00 PM','07:00 PM - 08:00 PM',
                '08:00 PM - 09:00 PM'];
            $existing_booking_slots = [];
            foreach ($bookings as $key => $booking){
                $start_time = strtotime($booking['start_time']);
                $start_time = date('h:i A', $start_time);
                $end_time = strtotime($booking['end_time']);
                $end_time = date('h:i A', $end_time);
                $existing_booking_slots[] = $start_time.' - '.$end_time;
            }
            $available_slots = array_diff($time_slots, $existing_booking_slots);
            foreach ($available_slots as $key => $slot){
                $time = $slot;
                $time = explode('-',$time);
                $start_time = $time[0];
                $end_time = $time[1];
                $avail_slot[] = $start_time.' - '.$end_time;
            }
//            $slots = [];
//            foreach($avail_slot as $index => $time){
////                $object = new \stdClass();
//                $object  = (object)$time;
//                $slots[] = $object;
//            }

            $available_slots = array_values($avail_slot);
            return $this->sendResponse($available_slots , 'Available Slots against date');
        }catch (\Exception $exception){
            return $this->sendCatchResponse($exception->getMessage(), []);
        }
    }

    public function storeBooking(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'start_date' => 'required',
//                'end_date' => 'required',
                'start_time' => 'required',
                'end_time' => 'required',
                'service_id' => 'required',
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $start_time =  date("H:i:s", strtotime($request->start_time));
            $end_time =  date("H:i:s", strtotime($request->end_time));
            $start_date = date('Y-m-d',strtotime($request->get('start_date')));
            $end_date = date('Y-m-d',strtotime($request->get('start_date')));
            $check_availability =  Booking::where('start_date',$start_date)->where('end_date',$end_date)
                ->where('start_time','=',$start_time)
                ->where('end_time','=',$end_time)
                ->count();
            if($check_availability){
                return $this->sendResponse([], 'Slot already booked, Please choose different slots');
            }
            $post_data = $request->all();
            $post_data['customer_id'] = Auth::user()->id;
            $post_data['status'] = 'Pending';
            $post_data['start_date'] =  date('Y-m-d',strtotime($request->start_date));
            $post_data['end_date'] =  date('Y-m-d',strtotime($request->start_date));
            $post_data['start_time'] = $start_time;
            $booking = Booking::create($post_data);
            if($booking)
                $booking = Booking::with('service')->find($booking->id);
                return $this->sendResponse($booking, 'Booking Created');

        }catch (\Exception $exception){
            return $this->sendCatchResponse($exception->getMessage(), []);

        }
    }

    public function totalOrders(){
      try{
          $customer_id = Auth::user()->id;
          $customer_orders = Booking::where('customer_id',$customer_id)->count();
          return $this->sendResponse($customer_orders, 'Total number of logged in user orders');
      }catch (\Exception $exception){
          return $this->sendCatchResponse($exception->getMessage(), []);
      }
    }

    public function jobsCompleted(){
      try{
          $customer_id = Auth::user()->id;
          $customer_orders = Booking::where('customer_id',$customer_id)->where('status','completed')->count();
          return $this->sendResponse($customer_orders, 'Total number of completed jobs');
      }catch (\Exception $exception){
          return $this->sendCatchResponse($exception->getMessage(), []);
      }
    }

    public function upcomingAppointments(){
        try{
            $customer_id = Auth::user()->id;
            $today_date = Carbon::now()->toDateString();
            $upcoming_appointments = Booking::where('customer_id',$customer_id)->where('status','pending')
                ->where('start_date','>',$today_date)->with(['customer','service'])->get();
            $result_array = [];
            foreach ($upcoming_appointments as $appointment){
                $result_array[] = [
                    'customer_name' => $appointment->customer->f_name.' '.$appointment->customer->l_name,
                    'phone_no' => $appointment->customer->phon_no,
                    'service' => $appointment->service->name,
                    'service_image' => $path = url('/assets/cutting').'/'.'1.jpg',
                    'appointment_date' => date('d-M-y',strtotime($appointment->start_date)).' '.$appointment->start_time.' - '.$appointment->end_time,
                ];
            }
            return $this->sendResponse($result_array, 'Upcoming Appointments list');
        }catch (\Exception $exception){
            return $this->sendCatchResponse($exception->getMessage(), []);
        }
    }

    public function previousAppointments(){
        try{
            $customer_id = Auth::user()->id;
            $today_date = Carbon::now()->toDateString();
            $upcoming_appointments = Booking::where('customer_id',$customer_id)
                ->where('start_date','<',$today_date)->with(['customer','service'])->get();
            $result_array = [];
            foreach ($upcoming_appointments as $appointment){
                $result_array[] = [
                    'customer_name' => $appointment->customer->f_name.' '.$appointment->customer->l_name,
                    'phone_no' => $appointment->customer->phon_no,
                    'service' => $appointment->service->name,
                    'service_image' => $path = url('/assets/cutting').'/'.'1.jpg',
                    'appointment_date' => date('d-M-y',strtotime($appointment->start_date)).' '.$appointment->start_time.' - '.$appointment->end_time,
                ];
            }
            return $this->sendResponse($result_array, 'Upcoming Appointments list');
        }catch (\Exception $exception){
            return $this->sendCatchResponse($exception->getMessage(), []);
        }
    }
}
