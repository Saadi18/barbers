<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Services;
use Illuminate\Http\Request;

class ServicesController extends BaseController
{
    public function index(){
        try {
            $bookings = Services::where('status','active')->get();
            return $this->sendResponse($bookings, 'Services Listed');
        }catch (\Exception $exception){
            return $this->sendCatchResponse($exception->getMessage(), []);
        }
    }

    public function detail($id){
        try {
            $booking = Services::find($id);
            if($booking)
                return $this->sendResponse($booking, 'Service Detail Listed');
            return $this->sendResponse($booking, 'No Data Found');
        }catch (\Exception $exception){
            return $this->sendCatchResponse($exception->getMessage(), []);
        }
    }
}
