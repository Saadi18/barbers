<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>
        Barber's
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://cdn.bootcss.com/webfont/1.6.16/webfontloader.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <!--begin::Page Vendors -->
    <link href="{{asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet"
          type="text/css"/>
    <!--end::Page Vendors -->
    <link href="{{asset('assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{asset('assets/demo/default/media/img/logo/favicon.ico')}}"/>
</head>

@if(url('login'))
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-3" id="m_login" style="background-image: url({{asset('assets/app/media/img//bg/bg-2.jpg')}});">
            @yield('login_content')
        </div>
        </div>
    <!-- end::Scroll Top -->            <!-- begin::Quick Nav -->
    @section('scripts')
        <!-- begin::Quick Nav -->
        <!--begin::Base Scripts -->
        <script src="{{asset('assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
        <!--end::Base Scripts -->
        <!--begin::Page Snippets -->
        <script src="{{asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
        <!--end::Page Snippets -->
    @show
        @stack('javascript_section')
    </body>
    @else
    <!-- end::Head -->
    <!--begin::body-->
    <body
        class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
    {{--    including header filer--}}
    @section('header')
        @include('includes.header')
    @show

    <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        {{--        including sidebar file--}}
        @section('sidebar')
            @include('includes.sidebar')
        @show
        <!-- end left side bar -->
            <!-- BEGIN: Subheader -->
            {{--    @include('partials.notification')--}}
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                @section('sub_header')
                    @include('includes.sub_header')
                @show
                {{--        end subheader--}}
                <div class="m-content">
                    @yield('content')
                </div>
            </div>
        </div>
        <!-- end:: Body -->
        @section('footer')
            @include('includes.footer')
        @show
        {{--    footer ends--}}
    </div>
    <!-- end:: pgae -->
    <!-- begin::Scroll Top -->
    <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500"
         data-scroll-speed="300">
        <i class="la la-arrow-up"></i>
    </div>
    <!-- end::Scroll Top -->            <!-- begin::Quick Nav -->
    @section('scripts')
        <!-- begin::Quick Nav -->
        <!--begin::Base Scripts -->
        <script src="{{asset('assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
        <!--end::Base Scripts -->
        <!--begin::Page Vendors -->
        <script src="{{asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}"
                type="text/javascript"></script>
        <!--end::Page Vendors -->
        <!--begin::Page Snippets -->
        <script src="{{asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>
        <!--end::Page Snippets -->
    @show
    @stack('javascript_section')
    </body>
@endif
</html>
